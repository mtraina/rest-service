(ns rest-service.database
  (:require [rest-service.mgdb :as mgdb]))

(defn find-all-documents [] (mgdb/find-all-documents))

(defn find-document [id] (mgdb/find-document id))

(defn create-document [document] (mgdb/create-document document))

(defn update-document [id document] (mgdb/update-document id document))

(defn delete-document [id] (mgdb/delete-document id))
