(ns rest-service.handler
      (:use compojure.core)
      (:require [compojure.handler :as handler]
                   [ring.middleware.json :as middleware]
                   [compojure.route :as route]
                   [rest-service.core :as core]))

    (defroutes app-routes
      (routes
        (context "/document" []
          (routes
            (GET  "/" [] (core/get-all-documents))
            (POST "/" {body :body} (core/create-document body))

            (context "/:id" [id]
              (routes
                (GET  "/" [] (core/get-document id))
                (PUT "/" {body :body} (core/update-document id body))
                (DELETE "/" [] (core/delete-document id))
                (PUT "/merge" {body :body} (core/merge-document id body)))))))

      (route/not-found "Not Found"))

    (def app
        (-> (handler/api app-routes)
            (middleware/wrap-json-body)
            (middleware/wrap-json-response)))
