(ns rest-service.mgdb
  (:import (org.bson.types ObjectId))
  (:require [monger.core :as mg]
               [monger.collection :as mc]
               [monger.json]))

  (def db "documents-db")
  (def collection "documents")
  (mg/connect!)
  ;(mg/connect {:port 37017})
  ;(mg/disconnect!)
  (mg/set-db! (mg/get-db db))

  (defn find-all-documents [] (mc/find-maps collection))

  (find-all-documents)

  (defn find-document [id] (mc/find-one-as-map collection {:_id (ObjectId. id)}))

  (defn create-document [document]
    (let [oid (ObjectId.)]
      (mc/insert-and-return collection (assoc document :_id oid))))

  (defn update-document [id document]
    (let [oid (ObjectId. id)]
      (mc/update-by-id collection oid document))
      (find-document id))


  (defn delete-document [id]
    (let [oid (ObjectId. id)]
      (mc/remove-by-id collection oid)))
