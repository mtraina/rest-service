(ns rest-service.core
  (:use [clojure.tools.logging :only (info)])
  (:require [rest-service.database :as db]
               ;[cheshire.core :refer :all]
               [ring.util.response :refer :all]
               [clojure.tools.logging :as log]))

(defn get-all-documents [] (db/find-all-documents))

(defn get-document [id] (response (db/find-document id)))

(defn create-document [document] {:status 201 :body (db/create-document document)})

(defn update-document [id document] (response (db/update-document id document)))

(defn delete-document [id] (db/delete-document id) {:status 204})

(defn merge-document [id document]
  (let [existing-document (db/find-document id)]
    ;(log/info "existing-document: " existing-document)
    (response (merge existing-document document))))
