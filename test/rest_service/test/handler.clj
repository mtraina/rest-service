(ns rest-service.test.handler
  (:use clojure.test
        ring.mock.request
        rest-service.handler)
  (:require [rest-service.core :as core]))

(deftest test-handler

  (testing "document route"

    (with-redefs [core/get-all-documents (fn [] {:status 200 :body []})]

      (let [response (app (request :get "/document"))]
        (print response)
        (is (= (:status response) 200))
        (is (= (:body response) "[]")))))

  (testing "not-found route"
    (let [response (app (request :get "/invalid"))]
      (is (= (:status response) 404)))))
