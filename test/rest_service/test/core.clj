(ns rest-service.test.core
  (:use clojure.test
          ring.mock.request
          rest-service.handler)
  (:require [rest-service.database :as db]
               [rest-service.core :as core]
               [clojure.tools.logging :as log]))

  (def dummy-document {:id 999, :title "dummy-document", :type "book", :active true, :date "01/01/2014"})

  (deftest test-core

    (testing "get documents"
      (with-redefs [db/find-all-documents (fn [] [dummy-document])]
        (let [documents (db/find-all-documents)]
          (is (= documents [dummy-document])))))

    (testing "merge document"
      (with-redefs [db/find-document (fn [id] dummy-document)]
        (def description "a brand new document")

        (let [merged-document (:body (core/merge-document 999 {:description description}))]
          (log/info "merged-document: " merged-document)
          (is (= description (:description merged-document)))))))
