REST API project, adapted from:
http://blog.interlinked.org/programming/clojure_rest.html

It requires an already running default (address 127.0.0.1, port 27017) instance of mongodb.

Compile and run with

"lein ring server-headless"

To run the test type

"lein test"

Once is up and running you can interact with the following commands:

# Get all documents
curl http://localhost:3000/document

# Create a document
curl -X POST -H 'Content-Type: application/json' -d '{"title":"doc", "type":"book", "active":true, "date":"01/01/2014"}' http://localhost:3000/document

# Update document
curl -X PUT -H 'Content-Type: application/json' -d '{"title":"doc", "type":"magazine", "active":true, "date":"01/01/2014"}' http://localhost:3000/document/<ID>

# Delete document
curl -X DELETE http://localhost:3000/document/<ID>

# Merge document
curl -X PUT -H 'Content-Type: application/json' -d '{"description":"not really interesting doc"}' http://localhost:3000/document/<ID>/merge
