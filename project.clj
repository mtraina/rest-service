(defproject rest-service "0.1.0-SNAPSHOT"
  :description "REST service for documents"
  :dependencies [[org.clojure/clojure "1.5.1"]
                [compojure "1.1.6"]
                [ring/ring-json "0.2.0"]
                [c3p0/c3p0 "0.9.1.2"]
                [org.clojure/java.jdbc "0.2.3"]
                [com.h2database/h2 "1.3.174"]
				        [cheshire "5.3.1"]
                [org.clojure/tools.logging "0.2.3"]
                [com.novemberain/monger "1.7.0"]]

  :plugins [[lein-ring "0.8.10"]
            [lein-embongo "0.2.1"]]

  :ring {:handler rest-service.handler/app}

  :embongo {:port 37017
            :data-dir "mongo-data-files"}

  :profiles
    {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
